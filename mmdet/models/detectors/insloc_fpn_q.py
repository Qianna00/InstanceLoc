import time

import numpy as np
import torch
import torch.nn as nn
from mmcv import Config
from mmcv.cnn import normal_init

from mmdet.core.bbox.iou_calculators import build_iou_calculator
from mmdet.core import bbox2result, bbox2roi, build_assigner, build_sampler
from ..builder import DETECTORS, build_backbone, build_head, build_neck, build_roi_extractor
from ..losses import accuracy
from .base import BaseDetector
from .insloc_fpn import InsLocFPN


@torch.no_grad()
def concat_all_gather(tensor):
    """
    Performs all_gather operation on the provided tensors.
    *** Warning ***: torch.distributed.all_gather has no gradient.
    """
    tensors_gather = [
        torch.ones_like(tensor)
        for _ in range(torch.distributed.get_world_size())
    ]
    torch.distributed.all_gather(tensors_gather, tensor, async_op=False)

    output = torch.cat(tensors_gather, dim=0)
    return output


@DETECTORS.register_module()
class InsLocFPNQ(InsLocFPN):

    def __init__(
        self,
        backbone,
        neck=None,
        rpn_head=None,
        roi_head=None,
        pool_with_gt=[True, True],
        shuffle_data=['img'],
        drop_rpn_k=True,
        momentum_cfg=None,
        train_cfg=None,
        test_cfg=None,
        pretrained=None,
        cross_patch=False,
        num_levels=4,
        level_loss_weights=[1.0, 1.0, 1.0, 1.0],
        box_replaced_with_gt=None,
        num_pos_per_instance=1,
        num_region_neg=-1
    ):
        super(InsLocFPNQ, self).__init__(
            backbone,
            neck=neck,
            rpn_head=rpn_head,
            roi_head=roi_head,
            pool_with_gt=pool_with_gt,
            shuffle_data=shuffle_data,
            drop_rpn_k=drop_rpn_k,
            momentum_cfg=momentum_cfg,
            train_cfg=train_cfg,
            test_cfg=test_cfg,
            pretrained=pretrained,
            num_levels=num_levels,
            level_loss_weights=level_loss_weights,
            box_replaced_with_gt=box_replaced_with_gt,
            num_pos_per_instance=num_pos_per_instance,
            num_region_neg=num_region_neg)
        self.cross_patch = cross_patch

    def create_momentum(
        self,
        momentum_cfg,
    ):
        self.momentum_cfg = Config(momentum_cfg)
        self.K = self.momentum_cfg.K
        self.m = self.momentum_cfg.m
        self.T = self.momentum_cfg.T
        assert (len(self.level_loss_weights) == self.num_levels)

        if self.momentum_cfg is not None:
            queues = []
            for i in range(self.num_levels + 1):
                # Create queue
                self.register_buffer(
                    "queue",
                    torch.randn(self.momentum_cfg.dim, self.momentum_cfg.K))
                self.queue = nn.functional.normalize(self.queue, dim=0)
                queues.append(self.queue)
            self.register_buffer("queues", torch.stack(queues, 0))
            #self.queues = torch.stack(queues)
            self.register_buffer("queue_ptr", torch.zeros(1, dtype=torch.long))

        # Create momentum net
        self.generate_momentum_net()

    def fwd(
        self,
        img,
        img_metas,
        gt_labels,
        gt_bboxes,
        backbone,
        roi_head,
        neck=None,
        pool_with_gt=True,
        rpn_head=None,
        query_encoder=False,
    ):

        losses = dict()
        x = backbone(img)

        if neck is not None:
            x = neck(x)

        if pool_with_gt:
            proposal_list = gt_bboxes
        else:
            proposal_cfg = self.train_cfg.get('rpn_proposal', None)
            proposal_list = rpn_head.forward_train(
                x,
                img_metas,
                gt_bboxes,
                proposal_cfg=proposal_cfg,
            )
            # implement your strategy for getting 2nd stage bboxes
            proposal_list = self.get_stage2_bboxes(proposal_list, gt_bboxes)

        if self.box_replaced_with_gt is not None and query_encoder:
            box_replaced_with_gt = self.box_replaced_with_gt
        else:
            box_replaced_with_gt = None

        logits, logits_patch = roi_head.forward_train(x, img_metas, proposal_list, gt_bboxes,
                                                      gt_labels, box_replaced_with_gt)
        return logits, logits_patch

    def forward_train(self,
                      img,
                      img_metas,
                      gt_bboxes,
                      gt_labels,
                      gt_bboxes_ignore=None,
                      gt_masks=None,
                      binary_masks=None,
                      proposals=None,
                      shifted_bboxes=None,
                      target_data=None,
                      **kwargs):
        losses = dict()
        batch_size, num_ins_per_img, _ = gt_bboxes.shape
        assert (num_ins_per_img == gt_labels.shape[-1])
        assert (num_ins_per_img == 1)

        if not isinstance(gt_bboxes, list):
            gt_bboxes = [each for each in gt_bboxes]

        if shifted_bboxes is not None:
            if not isinstance(shifted_bboxes, list):
                shifted_bboxes = [each for each in shifted_bboxes]

        if not isinstance(gt_labels, list):
            gt_labels = [each for each in gt_labels]

        # compute query features
        logits_q, logits_q_patch = self.fwd(
            img=img,
            img_metas=img_metas,
            gt_labels=gt_labels,
            gt_bboxes=gt_bboxes,
            backbone=self.backbone,
            roi_head=self.roi_head,
            neck=self.neck,
            pool_with_gt=self.pool_with_gt[0],
            rpn_head=self.rpn_head,
            query_encoder=True)
        logits_q = nn.functional.normalize(logits_q, dim=1)
        # logits_q_patch = torch.cat(logits_q_patch, dim=1)  # batch_size * 4 * feat_dim
        # logits_q_patch = nn.functional.normalize(logits_q_patch, dim=-1)

        num_feat_levels = logits_q.shape[0] // (
            batch_size * self.num_pos_per_instance)
        assert (self.num_levels == num_feat_levels)
        logits_q = logits_q.view(batch_size * self.num_pos_per_instance,
                                 num_feat_levels, -1)

        img_k = target_data['img']
        img_metas_k = target_data['img_metas']
        gt_bboxes_k = target_data['gt_bboxes']
        gt_labels_k = target_data['gt_labels']

        # compute key features
        with torch.no_grad():  # no gradient to keys
            self._momentum_update_key_encoder()  # update the key encoder

            # shuffle for making use of BN
            shuffle_idx = 'idx' in self.shuffle_data
            shuffle_bbox = 'bbox' in self.shuffle_data
            img_k, shuffle_gt_labels_k, shuffle_gt_bboxes_k, idx_unshuffle = self._batch_shuffle_ddp(
                img_k, gt_labels_k if shuffle_idx else None,
                gt_bboxes_k if shuffle_bbox else None)

            if shuffle_idx:
                gt_labels_k = [each for each in shuffle_gt_labels_k]
            else:
                gt_labels_k = [each for each in gt_labels_k]

            if shuffle_bbox:
                gt_bboxes_k = [each for each in shuffle_gt_bboxes_k]
            else:
                gt_bboxes_k = [each for each in gt_bboxes_k]

            logits_k, logits_k_patch = self.fwd(
                img=img_k,
                img_metas=img_metas_k,
                gt_labels=gt_labels_k,
                gt_bboxes=gt_bboxes_k,
                backbone=self.backbone_k,
                roi_head=self.roi_head_k,
                neck=self.neck_k,
                pool_with_gt=self.pool_with_gt[1],
                rpn_head=self.rpn_head_k,
                query_encoder=False)
            logits_k = nn.functional.normalize(logits_k, dim=1)
            logits_k = logits_k.view(batch_size, num_feat_levels, -1)
            logits_k_patch = torch.cat(logits_k_patch, dim=1)  # batch_size * 4 * feat_dim
            logits_k_patch = nn.functional.normalize(logits_k_patch, dim=-1)
            num_patch = logits_k_patch.shape[1]
            l_patch_list = list()
            for patch_idx in range(num_patch):
                l_patch = torch.einsum('nc,nc->n', [logits_q[:, num_feat_levels-1, :],
                                       logits_k_patch[:, patch_idx, :]])
                l_patch_list.append(l_patch.unsqueeze(1))
            l_patches = torch.cat(l_patch_list, dim=1)
            l_patch_max, l_patch_argmax = torch.max(l_patches, dim=1)
            logits_k_patch = logits_k_patch[range(0, batch_size), l_patch_argmax, :]

            if self.cross_patch:
                logits_q_patch = torch.cat(logits_q_patch, dim=1)  # batch_size * 4 * feat_dim
                logits_q_patch = nn.functional.normalize(logits_q_patch, dim=-1)
                l_patch_list_b = list()
                for patch_idx in range(num_patch):
                    l_patch_b = torch.einsum('nc,nc->n', [logits_q_patch[:, patch_idx, :],
                                                        logits_k[:, num_feat_levels - 1, :]])
                    l_patch_list_b.append(l_patch_b.unsqueeze(1))
                l_patches_b = torch.cat(l_patch_list_b, dim=1)
                l_patch_max_b, l_patch_argmax_b = torch.max(l_patches_b, dim=1)
                # logits_k_patch_b = logits_q_patch[range(0, batch_size), l_patch_argmax_b, :]
                logits_k_patch = torch.where(l_patch_max.unsqueeze(1).expand(logits_k_patch.size())
                                             > l_patch_max_b.unsqueeze(1).expand(logits_k_patch.size()),
                                             logits_k_patch, logits_k[:, num_feat_levels - 1, :])
                l_patch_max = torch.where(l_patch_max > l_patch_max_b, l_patch_max, l_patch_max_b)

            # undo shuffle
            logits_k = self._batch_unshuffle_ddp(logits_k, idx_unshuffle).view(
                batch_size, num_feat_levels, -1)
            logits_k_patch = self._batch_unshuffle_ddp(logits_k_patch, idx_unshuffle).view(
                batch_size, -1)
            if self.num_pos_per_instance > 1:
                batch_size_k = logits_k.shape[0]
                batch_size_q = logits_q.shape[0]
                assert (batch_size_q //
                        batch_size_k == self.num_pos_per_instance)
                repeated_logits_k = torch.repeat_interleave(
                    logits_k.unsqueeze(1), self.num_pos_per_instance,
                    1).view(-1, num_feat_levels, logits_q.shape[-1])
                """repeated_logits_k_patch = torch.repeat_interleave(
                    logits_k_patch.unsqueeze(1), self.num_pos_per_instance,
                    1).view(-1, num_feat_levels, logits_q.shape[-1])"""
            else:
                repeated_logits_k = logits_k
                # repeated_logits_k_patch = logits_k_patch

        for level_idx in range(num_feat_levels):
            # compute logits
            # Einstein sum is more intuitive
            # positive logits: Nx1
            l_pos = torch.einsum('nc,nc->n', [
                logits_q[:, level_idx, :], repeated_logits_k[:, level_idx, :]
            ]).unsqueeze(-1)
            # negative logits: NxK
            l_neg = torch.einsum('nc,ck->nk', [
                logits_q[:, level_idx, :],
                self.queues[level_idx].clone().detach()
            ])

            # logits: Nx(1+K)
            logits = torch.cat([l_pos, l_neg], dim=1)

            # apply temperature
            logits /= self.T

            # labels: positive key indicators
            labels = torch.zeros(logits.shape[0], dtype=torch.long).cuda()

            # ce loss
            loss_cls = self.level_loss_weights[
                level_idx] * nn.functional.cross_entropy(logits, labels)
            acc = accuracy(logits, labels)

            losses.update({
                f'loss_cls_{level_idx}': loss_cls,
                f'acc_{level_idx}': acc
            })
            #loss_cls=loss_cls, acc=acc)
        l_patch_pos = l_patch_max.unsqueeze(-1)
        l_patch_neg = torch.einsum('nc,ck->nk', [
            logits_q[:, -1, :],
            self.queues[-1].clone().detach()
        ])
        logits_patch = torch.cat([l_patch_pos, l_patch_neg], dim=1)
        logits_patch /= self.T
        labels_patch = torch.zeros(logits_patch.shape[0], dtype=torch.long).cuda()
        loss_patch_cls = nn.functional.cross_entropy(logits_patch, labels_patch)
        acc_patch = accuracy(logits_patch, labels_patch)
        losses.update({
            f'loss_cls_patch': loss_patch_cls,
            f'acc_patch': acc_patch
        })


        # dequeue and enqueue
        self._dequeue_and_enqueue(logits_k, logits_k_patch)

        return losses

    @torch.no_grad()
    def _dequeue_and_enqueue(self, keys, keys_patch):
        # gather keys before updating queue
        keys = concat_all_gather(keys)
        keys_patch = concat_all_gather(keys_patch)

        batch_size = keys.shape[0]

        ptr = int(self.queue_ptr)
        assert self.K % batch_size == 0  # for simplicity

        self.queues[:-1, :, ptr:ptr + batch_size] = keys.permute(1, 2, 0)
        self.queues[-1, :, ptr:ptr + batch_size] = keys_patch.permute(1, 0)
        ptr = (ptr + batch_size) % self.K  # move pointer

        self.queue_ptr[0] = ptr

    @torch.no_grad()
    def _batch_shuffle_ddp(self, x, y=None, z=None):
        """
        Batch shuffle, for making use of BatchNorm.
        *** Only support DistributedDataParallel (DDP) model. ***
        """
        # gather from all gpus
        batch_size_this = x.shape[0]
        x_gather = concat_all_gather(x)

        if y is not None:
            y_gather = concat_all_gather(y)
            assert (y_gather.shape[0] == x_gather.shape[0])
        else:
            y_gather = None

        if z is not None:
            z_gather = concat_all_gather(z)
            assert (z_gather.shape[0] == x_gather.shape[0])
        else:
            z_gather = None
        batch_size_all = x_gather.shape[0]

        num_gpus = batch_size_all // batch_size_this

        # random shuffle index
        idx_shuffle = torch.randperm(batch_size_all).cuda()

        # broadcast to all gpus
        torch.distributed.broadcast(idx_shuffle, src=0)

        # index for restoring
        idx_unshuffle = torch.argsort(idx_shuffle)

        # shuffled index for this gpu
        gpu_idx = torch.distributed.get_rank()
        idx_this = idx_shuffle.view(num_gpus, -1)[gpu_idx]

        return x_gather[idx_this], None if y_gather is None else y_gather[
            idx_this], None if z_gather is None else z_gather[
                idx_this], idx_unshuffle

    @torch.no_grad()
    def _batch_unshuffle_ddp(self, x, idx_unshuffle):
        """
        Undo batch shuffle.
        *** Only support DistributedDataParallel (DDP) model. ***
        """
        # gather from all gpus
        batch_size_this = x.shape[0]
        x_gather = concat_all_gather(x)
        batch_size_all = x_gather.shape[0]

        num_gpus = batch_size_all // batch_size_this

        # restored index for this gpu
        gpu_idx = torch.distributed.get_rank()
        idx_this = idx_unshuffle.view(num_gpus, -1)[gpu_idx]

        return x_gather[idx_this]


@DETECTORS.register_module()
class AnotherInsLocFPNQ(InsLocFPNQ):

    def __init__(
        self,
        backbone,
        neck=None,
        rpn_head=None,
        roi_head=None,
        extra_branch=None,
        pool_with_gt=[True, True],
        shuffle_data=['img'],
        drop_rpn_k=True,
        momentum_cfg=None,
        train_cfg=None,
        test_cfg=None,
        pretrained=None,
        cross_patch=True,
        num_levels=4,
        level_loss_weights=[1.0, 1.0, 1.0, 1.0],
        box_replaced_with_gt=None,
        num_pos_per_instance=1,
        num_region_neg=-1
    ):
        super(AnotherInsLocFPNQ, self).__init__(
            backbone,
            neck=neck,
            rpn_head=rpn_head,
            roi_head=roi_head,
            pool_with_gt=pool_with_gt,
            shuffle_data=shuffle_data,
            drop_rpn_k=drop_rpn_k,
            momentum_cfg=momentum_cfg,
            train_cfg=train_cfg,
            test_cfg=test_cfg,
            pretrained=pretrained,
            cross_patch=cross_patch,
            num_levels=num_levels,
            level_loss_weights=level_loss_weights,
            box_replaced_with_gt=box_replaced_with_gt,
            num_pos_per_instance=num_pos_per_instance,
            num_region_neg=num_region_neg)
        self.bbox_roi_extractor_g = build_roi_extractor(extra_branch['bbox_roi_extractor_g'])
        self.bbox_roi_extractor_l = build_roi_extractor(extra_branch['bbox_roi_extractor_l'])
        self.extra_bbox_head = build_head(extra_branch['bbox_head'])
        self.extra_bbox_head.init_weights()

    def fwd(
        self,
        img,
        img_metas,
        gt_labels,
        gt_bboxes,
        backbone,
        roi_head,
        neck=None,
        pool_with_gt=True,
        rpn_head=None,
        query_encoder=False,
    ):

        x = backbone(img)

        if neck is not None:
            x = neck(x)

        if pool_with_gt:
            proposal_list = gt_bboxes
        else:
            proposal_cfg = self.train_cfg.get('rpn_proposal', None)
            if len(x) == self.num_levels:
                proposal_list = rpn_head.forward_train(
                    x,
                    img_metas,
                    gt_bboxes,
                    proposal_cfg=proposal_cfg,
                )
            else:
                proposal_list = rpn_head.forward_train(
                    x[:-1],
                    img_metas,
                    gt_bboxes,
                    proposal_cfg=proposal_cfg,
                )
            # implement your strategy for getting 2nd stage bboxes
            proposal_list = self.get_stage2_bboxes(proposal_list, gt_bboxes)

        if self.box_replaced_with_gt is not None and query_encoder:
            box_replaced_with_gt = self.box_replaced_with_gt
        else:
            box_replaced_with_gt = None

        logits = roi_head.forward_train(x[:-1], img_metas, proposal_list, gt_bboxes,
                                        gt_labels, box_replaced_with_gt)
        gt_rois = bbox2roi(gt_bboxes)
        bbox_feat_g = self.bbox_roi_extractor_g((x[-1], ), gt_rois)
        # bbox_feat_g_list = []
        shape_w, shape_h = bbox_feat_g.shape[-2:]
        shape_w_split = int(shape_w / 2)
        shape_h_split = int(shape_h / 2)
        bbox_feats_patch_list = [bbox_feat_g[:, :, :shape_w_split, :shape_h_split],
                                 bbox_feat_g[:, :, shape_w_split:, :shape_h_split],
                                 bbox_feat_g[:, :, :shape_w_split, shape_h_split:],
                                 bbox_feat_g[:, :, shape_w_split:, shape_h_split:]]
        bbox_feat_g = torch.cat(bbox_feats_patch_list, dim=0)
        bbox_feat_l = self.bbox_roi_extractor_l((x[-1], ), gt_rois)
        logits_patch_g, _ = self.extra_bbox_head(bbox_feat_g)
        logits_patch_l, _ = self.extra_bbox_head(bbox_feat_l)
        return logits, logits_patch_g, logits_patch_l

    def forward_train(self,
                      img,
                      img_metas,
                      gt_bboxes,
                      gt_labels,
                      gt_bboxes_ignore=None,
                      gt_masks=None,
                      binary_masks=None,
                      proposals=None,
                      shifted_bboxes=None,
                      target_data=None,
                      **kwargs):
        losses = dict()
        batch_size, num_ins_per_img, _ = gt_bboxes.shape
        assert (num_ins_per_img == gt_labels.shape[-1])
        assert (num_ins_per_img == 1)

        if not isinstance(gt_bboxes, list):
            gt_bboxes = [each for each in gt_bboxes]

        if shifted_bboxes is not None:
            if not isinstance(shifted_bboxes, list):
                shifted_bboxes = [each for each in shifted_bboxes]

        if not isinstance(gt_labels, list):
            gt_labels = [each for each in gt_labels]

        # compute query features
        logits_q, logits_q_patch_g, logits_q_patch_l = self.fwd(
            img=img,
            img_metas=img_metas,
            gt_labels=gt_labels,
            gt_bboxes=gt_bboxes,
            backbone=self.backbone,
            roi_head=self.roi_head,
            neck=self.neck,
            pool_with_gt=self.pool_with_gt[0],
            rpn_head=self.rpn_head,
            query_encoder=True)
        logits_q = nn.functional.normalize(logits_q, dim=1)
        logits_q_patch_l = nn.functional.normalize(logits_q_patch_l, dim=1)

        num_feat_levels = logits_q.shape[0] // (
            batch_size * self.num_pos_per_instance)
        assert (self.num_levels == num_feat_levels)
        logits_q = logits_q.view(batch_size * self.num_pos_per_instance,
                                 num_feat_levels, -1)

        img_k = target_data['img']
        img_metas_k = target_data['img_metas']
        gt_bboxes_k = target_data['gt_bboxes']
        gt_labels_k = target_data['gt_labels']

        # compute key features
        with torch.no_grad():  # no gradient to keys
            self._momentum_update_key_encoder()  # update the key encoder

            # shuffle for making use of BN
            shuffle_idx = 'idx' in self.shuffle_data
            shuffle_bbox = 'bbox' in self.shuffle_data
            img_k, shuffle_gt_labels_k, shuffle_gt_bboxes_k, idx_unshuffle = self._batch_shuffle_ddp(
                img_k, gt_labels_k if shuffle_idx else None,
                gt_bboxes_k if shuffle_bbox else None)

            if shuffle_idx:
                gt_labels_k = [each for each in shuffle_gt_labels_k]
            else:
                gt_labels_k = [each for each in gt_labels_k]

            if shuffle_bbox:
                gt_bboxes_k = [each for each in shuffle_gt_bboxes_k]
            else:
                gt_bboxes_k = [each for each in gt_bboxes_k]

            logits_k, logits_k_patch_g, logits_k_patch_l = self.fwd(
                img=img_k,
                img_metas=img_metas_k,
                gt_labels=gt_labels_k,
                gt_bboxes=gt_bboxes_k,
                backbone=self.backbone_k,
                roi_head=self.roi_head_k,
                neck=self.neck_k,
                pool_with_gt=self.pool_with_gt[1],
                rpn_head=self.rpn_head_k,
                query_encoder=False)
            logits_k = nn.functional.normalize(logits_k, dim=1)
            logits_k = logits_k.view(batch_size, num_feat_levels, -1)
            logits_k_patch_g = nn.functional.normalize(logits_k_patch_g, dim=1)
            logits_k_patch_g = torch.cat([patch.unsqueeze(1) for patch in torch.chunk(logits_k_patch_g, 4)], dim=1)
            num_patch = logits_k_patch_g.shape[1]
            l_patch_list = list()
            for patch_k_idx in range(num_patch):
                l_patch = torch.einsum('nc,nc->n', [logits_q_patch_l,
                                                    logits_k_patch_g[:, patch_k_idx, :]])
                l_patch_list.append(l_patch.unsqueeze(1))
            l_patches = torch.cat(l_patch_list, dim=1)
            l_patch_max, l_patch_argmax = torch.max(l_patches, dim=1)

            logits_k_patch = logits_k_patch_g[range(0, batch_size), l_patch_argmax, :]

            if self.cross_patch:
                logits_q_patch_g = nn.functional.normalize(logits_q_patch_g, dim=1)
                logits_q_patch_g = torch.cat([patch.unsqueeze(1) for patch in torch.chunk(logits_q_patch_g, 4)], dim=1)
                logits_k_patch_l = nn.functional.normalize(logits_k_patch_l, dim=1)
                l_patch_list_b = list()
                for patch_idx in range(num_patch):
                    l_patch_b = torch.einsum('nc,nc->n', [logits_q_patch_g[:, patch_idx, :],
                                                        logits_k_patch_l])
                    l_patch_list_b.append(l_patch_b.unsqueeze(1))
                l_patches_b = torch.cat(l_patch_list_b, dim=1)
                l_patch_max_b, l_patch_argmax_b = torch.max(l_patches_b, dim=1)
                # logits_k_patch_b = logits_q_patch[range(0, batch_size), l_patch_argmax_b, :]
                logits_k_patch = torch.where(l_patch_max.unsqueeze(1).expand(logits_k_patch.size())
                                             > l_patch_max_b.unsqueeze(1).expand(logits_k_patch.size()),
                                             logits_k_patch, logits_k_patch_l)
                l_patch_max = torch.where(l_patch_max > l_patch_max_b, l_patch_max, l_patch_max_b)

            # undo shuffle
            logits_k = self._batch_unshuffle_ddp(logits_k, idx_unshuffle).view(
                batch_size, num_feat_levels, -1)
            logits_k_patch = self._batch_unshuffle_ddp(logits_k_patch, idx_unshuffle).view(
                batch_size, -1)
            if self.num_pos_per_instance > 1:
                batch_size_k = logits_k.shape[0]
                batch_size_q = logits_q.shape[0]
                assert (batch_size_q //
                        batch_size_k == self.num_pos_per_instance)
                repeated_logits_k = torch.repeat_interleave(
                    logits_k.unsqueeze(1), self.num_pos_per_instance,
                    1).view(-1, num_feat_levels, logits_q.shape[-1])
                """repeated_logits_k_patch = torch.repeat_interleave(
                    logits_k_patch.unsqueeze(1), self.num_pos_per_instance,
                    1).view(-1, num_feat_levels, logits_q.shape[-1])"""
            else:
                repeated_logits_k = logits_k
                # repeated_logits_k_patch = logits_k_patch

        for level_idx in range(num_feat_levels):
            # compute logits
            # Einstein sum is more intuitive
            # positive logits: Nx1
            l_pos = torch.einsum('nc,nc->n', [
                logits_q[:, level_idx, :], repeated_logits_k[:, level_idx, :]
            ]).unsqueeze(-1)
            # negative logits: NxK
            l_neg = torch.einsum('nc,ck->nk', [
                logits_q[:, level_idx, :],
                self.queues[level_idx].clone().detach()
            ])

            # logits: Nx(1+K)
            logits = torch.cat([l_pos, l_neg], dim=1)

            # apply temperature
            logits /= self.T

            # labels: positive key indicators
            labels = torch.zeros(logits.shape[0], dtype=torch.long).cuda()

            # ce loss
            loss_cls = self.level_loss_weights[
                level_idx] * nn.functional.cross_entropy(logits, labels)
            acc = accuracy(logits, labels)

            losses.update({
                f'loss_cls_{level_idx}': loss_cls,
                f'acc_{level_idx}': acc
            })
            #loss_cls=loss_cls, acc=acc)
        l_patch_pos = l_patch_max.unsqueeze(-1)
        l_patch_neg = torch.einsum('nc,ck->nk', [
            logits_q_patch_l,
            self.queues[-1].clone().detach()
        ])
        logits_patch = torch.cat([l_patch_pos, l_patch_neg], dim=1)
        logits_patch /= self.T
        labels_patch = torch.zeros(logits_patch.shape[0], dtype=torch.long).cuda()
        loss_patch_cls = nn.functional.cross_entropy(logits_patch, labels_patch)
        acc_patch = accuracy(logits_patch, labels_patch)
        losses.update({
            f'loss_cls_patch': loss_patch_cls,
            f'acc_patch': acc_patch
        })


        # dequeue and enqueue
        self._dequeue_and_enqueue(logits_k, logits_k_patch)

        return losses
