from .insloc_fpn import InsLocFPN
from .insloc_fpn_q import InsLocFPNQ, AnotherInsLocFPNQ
from .insloc_fpn_sim_label import InsLocFPNSimlarityLabel, InsLocFPNSimlarityLabelNew

__all__ = ['InsLocFPN', 'InsLocFPNQ', 'AnotherInsLocFPNQ', 'InsLocFPNSimlarityLabel', 'InsLocFPNSimlarityLabelNew']
