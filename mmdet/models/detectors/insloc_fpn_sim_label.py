import time

import numpy as np
import torch
import torch.nn as nn
from mmcv import Config
from mmcv.cnn import normal_init

from mmdet.core.bbox.iou_calculators import build_iou_calculator
# from mmdet.core import bbox2result, bbox2roi, build_assigner, build_sampler
from ..builder import DETECTORS, build_backbone, build_head, build_neck
from ..losses import accuracy
from .base import BaseDetector
from .insloc_fpn import InsLocFPN, concat_all_gather



@DETECTORS.register_module()
class InsLocFPNSimlarityLabel(InsLocFPN):

    def __init__(
        self,
        backbone,
        neck=None,
        rpn_head=None,
        roi_head=None,
        pool_with_gt=[True, True],
        shuffle_data=['img'],
        drop_rpn_k=True,
        momentum_cfg=None,
        train_cfg=None,
        test_cfg=None,
        pretrained=None,
        num_levels=4,
        level_loss_weights=[1.0, 1.0, 1.0, 1.0],
        sim_loss_weight=1.0,
        box_replaced_with_gt=None,
        num_pos_per_instance=1,
        num_region_neg=-1,
    ):
        super(InsLocFPNSimlarityLabel, self).__init__(
            backbone,
            neck=neck,
            rpn_head=rpn_head,
            roi_head=roi_head,
            pool_with_gt=pool_with_gt,
            shuffle_data=shuffle_data,
            drop_rpn_k=drop_rpn_k,
            momentum_cfg=momentum_cfg,
            train_cfg=train_cfg,
            test_cfg=test_cfg,
            pretrained=pretrained,
            num_levels=num_levels,
            level_loss_weights=level_loss_weights,
            box_replaced_with_gt=box_replaced_with_gt,
            num_pos_per_instance=num_pos_per_instance,
            num_region_neg=num_region_neg)
        self.sim_loss_weight = sim_loss_weight


    @torch.no_grad()
    def _momentum_update_key_encoder(self):
        self._momentum_update_net(self.backbone, self.backbone_k)
        # if self.with_neck:
        self._momentum_update_net(self.neck, self.neck_k)
        # if self.with_rpn and not self.drop_rpn_k:
        self._momentum_update_net(self.rpn_head, self.rpn_head_k)
        # if self.with_roi_head:
        self._momentum_update_net(self.roi_head, self.roi_head_k)

    def init_weights(self, pretrained=None):
        super(InsLocFPN, self).init_weights(pretrained)
        self.backbone.init_weights(pretrained=pretrained)
        # if self.with_neck:
        if self.neck is not None:
            if isinstance(self.neck, nn.Sequential):
                for m in self.neck:
                    m.init_weights()
            else:
                self.neck.init_weights()
        # if self.with_rpn:
        if self.rpn_head is not None:
            self.rpn_head.init_weights()
        # if self.with_roi_head:
        if self.roi_head is not None:
            self.roi_head.init_weights(pretrained)

    def fwd(
        self,
        img,
        img_metas,
        gt_labels,
        gt_bboxes,
        backbone,
        roi_head,
        neck=None,
        pool_with_gt=True,
        rpn_head=None,
        query_encoder=False,
        overlap_bboxes=None,
    ):

        losses = dict()
        x = backbone(img)

        if neck is not None:
            x = neck(x)

        if pool_with_gt:
            proposal_list = gt_bboxes
        else:
            proposal_cfg = self.train_cfg.get('rpn_proposal', None)
            proposal_list = rpn_head.forward_train(
                x,
                img_metas,
                gt_bboxes,
                proposal_cfg=proposal_cfg,
            )
            # implement your strategy for getting 2nd stage bboxes
            proposal_list = self.get_stage2_bboxes(proposal_list, gt_bboxes)

        if self.box_replaced_with_gt is not None and query_encoder:
            box_replaced_with_gt = self.box_replaced_with_gt
        else:
            box_replaced_with_gt = None

        logits, logits_patch = roi_head.forward_train(x, img_metas, proposal_list, gt_bboxes,
                                                      gt_labels, box_replaced_with_gt, overlap_bboxes)
        return logits, logits_patch

    def get_stage2_bboxes(self, proposal_list, gt_bboxes):
        assert (len(proposal_list) == len(gt_bboxes))
        num_imgs = len(gt_bboxes)
        # currently, we random choose one proposal for stage 2
        outs = []
        for i in range(num_imgs):
            proposal_i = proposal_list[i]
            idx = np.random.randint(0, proposal_i.shape[0], 1)
            outs.append(proposal_i[int(idx), :4][None, :])
        return outs

    def forward_train(self,
                      img,
                      img_metas,
                      gt_bboxes,
                      gt_labels,
                      sim_scores,
                      global_flag,
                      gt_bboxes_ignore=None,
                      gt_masks=None,
                      binary_masks=None,
                      proposals=None,
                      shifted_bboxes=None,
                      target_data=None,
                      **kwargs):
        losses = dict()
        batch_size, num_ins_per_img, _ = gt_bboxes.shape
        assert (num_ins_per_img == gt_labels.shape[-1])
        assert (num_ins_per_img == 1)

        if not isinstance(gt_bboxes, list):
            gt_bboxes = [each for each in gt_bboxes]

        if shifted_bboxes is not None:
            if not isinstance(shifted_bboxes, list):
                shifted_bboxes = [each for each in shifted_bboxes]

        if not isinstance(gt_labels, list):
            gt_labels = [each for each in gt_labels]

        # compute query features
        logits_q, logits_q_patch = self.fwd(
            img=img,
            img_metas=img_metas,
            gt_labels=gt_labels,
            gt_bboxes=gt_bboxes,
            backbone=self.backbone,
            roi_head=self.roi_head,
            neck=self.neck,
            pool_with_gt=self.pool_with_gt[0],
            rpn_head=self.rpn_head,
            query_encoder=True)
        logits_q = nn.functional.normalize(logits_q, dim=1)

        num_feat_levels = logits_q.shape[0] // (
                batch_size * self.num_pos_per_instance)
        assert (self.num_levels == num_feat_levels)
        logits_q = logits_q.view(batch_size * self.num_pos_per_instance,
                                 num_feat_levels, -1)
        local_flag = ~global_flag
        logits_q_patch = torch.cat(logits_q_patch, dim=1)  # batch_size * 4 * feat_dim
        logits_q_patch = logits_q_patch[global_flag]
        logits_q_patch = nn.functional.normalize(logits_q_patch, dim=-1)

        img_k = target_data['img']
        img_metas_k = target_data['img_metas']
        gt_bboxes_k = target_data['gt_bboxes']
        gt_labels_k = target_data['gt_labels']

        # compute key features
        with torch.no_grad():  # no gradient to keys
            self._momentum_update_key_encoder()  # update the key encoder

            # shuffle for making use of BN
            shuffle_idx = 'idx' in self.shuffle_data
            shuffle_bbox = 'bbox' in self.shuffle_data
            img_k, shuffle_gt_labels_k, shuffle_gt_bboxes_k, idx_unshuffle = self._batch_shuffle_ddp(
                img_k, gt_labels_k if shuffle_idx else None,
                gt_bboxes_k if shuffle_bbox else None)

            if shuffle_idx:
                gt_labels_k = [each for each in shuffle_gt_labels_k]
            else:
                gt_labels_k = [each for each in gt_labels_k]

            if shuffle_bbox:
                gt_bboxes_k = [each for each in shuffle_gt_bboxes_k]
            else:
                gt_bboxes_k = [each for each in gt_bboxes_k]

            logits_k, logits_k_patch = self.fwd(
                img=img_k,
                img_metas=img_metas_k,
                gt_labels=gt_labels_k,
                gt_bboxes=gt_bboxes_k,
                backbone=self.backbone_k,
                roi_head=self.roi_head_k,
                neck=self.neck_k,
                pool_with_gt=self.pool_with_gt[1],
                rpn_head=self.rpn_head_k,
                query_encoder=False)
            logits_k = nn.functional.normalize(logits_k, dim=1)
            logits_k = logits_k.view(batch_size, num_feat_levels, -1)
            logits_k_patch = torch.cat(logits_k_patch, dim=1)  # batch_size * 4 * feat_dim
            logits_k_patch = logits_k_patch[local_flag]
            logits_k_patch = nn.functional.normalize(logits_k_patch, dim=-1)

            # undo shuffle
            logits_k = self._batch_unshuffle_ddp(logits_k, idx_unshuffle).view(
                batch_size, num_feat_levels, -1)
            if self.num_pos_per_instance > 1:
                batch_size_k = logits_k.shape[0]
                batch_size_q = logits_q.shape[0]
                assert (batch_size_q //
                        batch_size_k == self.num_pos_per_instance)
                repeated_logits_k = torch.repeat_interleave(
                    logits_k.unsqueeze(1), self.num_pos_per_instance,
                    1).view(-1, num_feat_levels, logits_q.shape[-1])
            else:
                repeated_logits_k = logits_k

        for level_idx in range(num_feat_levels):
            # compute logits
            # Einstein sum is more intuitive
            # positive logits: Nx1
            l_pos = torch.einsum('nc,nc->n', [
                logits_q[:, level_idx, :], repeated_logits_k[:, level_idx, :]
            ]).unsqueeze(-1)
            # negative logits: NxK
            l_neg = torch.einsum('nc,ck->nk', [
                logits_q[:, level_idx, :],
                self.queues[level_idx].clone().detach()
            ])

            # logits: Nx(1+K)
            logits = torch.cat([l_pos, l_neg], dim=1)

            # apply temperature
            logits /= self.T

            # labels: positive key indicators
            labels = torch.zeros(logits.shape[0], dtype=torch.long).cuda()

            # ce loss
            loss_cls = self.level_loss_weights[
                           level_idx] * nn.functional.cross_entropy(logits, labels)
            acc = accuracy(logits, labels)

            losses.update({
                f'loss_cls_{level_idx}': loss_cls,
                f'acc_{level_idx}': acc
            })
            # loss_cls=loss_cls, acc=acc)

        logits_q_global = logits_q[local_flag]
        logits_k_global = logits_k[global_flag]
        sim_scores_pred = torch.zeros((batch_size, 4)).cuda()
        sim_scores_pred_list_q = list()
        num_patch_q = logits_q_patch.shape[1]
        for patch_idx in range(num_patch_q):
            sim_score_pred = torch.einsum('nc,nc->n', [logits_q_patch[:, patch_idx, :],
                                                       logits_k_global[:, num_feat_levels - 1, :].detach()])
            sim_scores_pred_list_q.append(sim_score_pred.unsqueeze(1))
        sim_scores_pred_q = torch.cat(sim_scores_pred_list_q, dim=1)
        sim_scores_pred_list_k = list()
        num_patch_k = logits_q_patch.shape[1]
        for patch_idx in range(num_patch_k):
            sim_score_pred = torch.einsum('nc,nc->n', [logits_q_global[:, num_feat_levels - 1, :],
                                                       logits_k_patch[:, patch_idx, :].detach()])
            sim_scores_pred_list_k.append(sim_score_pred.unsqueeze(1))
        sim_scores_pred_k = torch.cat(sim_scores_pred_list_k, dim=1)
        sim_scores_pred[global_flag] = sim_scores_pred_q
        sim_scores_pred[local_flag] = sim_scores_pred_k
        sim_scores_pred = nn.functional.softmax(sim_scores_pred, dim=1)
        loss_sim = self.sim_loss_weight * nn.functional.pairwise_distance(sim_scores_pred, sim_scores).mean()
        losses.update({
            f'loss_sim': loss_sim
        })

        # dequeue and enqueue
        self._dequeue_and_enqueue(logits_k)

        return losses

    @torch.no_grad()
    def _dequeue_and_enqueue(self, keys):
        # gather keys before updating queue
        keys = concat_all_gather(keys)

        batch_size = keys.shape[0]

        ptr = int(self.queue_ptr)
        assert self.K % batch_size == 0  # for simplicity

        self.queues[:, :, ptr:ptr + batch_size] = keys.permute(1, 2, 0)
        ptr = (ptr + batch_size) % self.K  # move pointer

        self.queue_ptr[0] = ptr



@DETECTORS.register_module()
class InsLocFPNSimlarityLabelNew(InsLocFPNSimlarityLabel):
    def fwd(
        self,
        img,
        img_metas,
        gt_labels,
        gt_bboxes,
        backbone,
        roi_head,
        neck=None,
        pool_with_gt=True,
        rpn_head=None,
        query_encoder=False,
        overlap_bboxes=None,
        overlap_flag=None
    ):

        losses = dict()
        x = backbone(img)

        if neck is not None:
            x = neck(x)

        if pool_with_gt:
            proposal_list = gt_bboxes
        else:
            proposal_cfg = self.train_cfg.get('rpn_proposal', None)
            proposal_list = rpn_head.forward_train(
                x,
                img_metas,
                gt_bboxes,
                proposal_cfg=proposal_cfg,
            )
            # implement your strategy for getting 2nd stage bboxes
            proposal_list = self.get_stage2_bboxes(proposal_list, gt_bboxes)

        if self.box_replaced_with_gt is not None and query_encoder:
            box_replaced_with_gt = self.box_replaced_with_gt
        else:
            box_replaced_with_gt = None

        if query_encoder:
            logits, logits_patch, logits_overlap = roi_head.forward_train(x, img_metas, proposal_list, gt_bboxes,
                                                                          gt_labels, box_replaced_with_gt,
                                                                          overlap_bboxes, overlap_flag)
            return logits, logits_patch, logits_overlap
        else:
            logits, logits_patch = roi_head.forward_train(x, img_metas, proposal_list, gt_bboxes,
                                                          gt_labels, box_replaced_with_gt)
            return logits, logits_patch

    def forward_train(self,
                      img,
                      img_metas,
                      gt_bboxes,
                      gt_labels,
                      overlap_bboxes,
                      overlap_flag,
                      sim_scores,
                      gt_bboxes_ignore=None,
                      gt_masks=None,
                      binary_masks=None,
                      proposals=None,
                      shifted_bboxes=None,
                      target_data=None,
                      **kwargs):
        losses = dict()
        batch_size, num_ins_per_img, _ = gt_bboxes.shape
        assert (num_ins_per_img == gt_labels.shape[-1])
        assert (num_ins_per_img == 1)

        if not isinstance(gt_bboxes, list):
            gt_bboxes = [each for each in gt_bboxes]

        if shifted_bboxes is not None:
            if not isinstance(shifted_bboxes, list):
                shifted_bboxes = [each for each in shifted_bboxes]

        if not isinstance(gt_labels, list):
            gt_labels = [each for each in gt_labels]

        overlap_flag = overlap_flag.squeeze()

        # compute query features
        logits_q, logits_q_patch, logits_overlap = self.fwd(
            img=img,
            img_metas=img_metas,
            gt_labels=gt_labels,
            gt_bboxes=gt_bboxes,
            backbone=self.backbone,
            roi_head=self.roi_head,
            neck=self.neck,
            pool_with_gt=self.pool_with_gt[0],
            rpn_head=self.rpn_head,
            query_encoder=True,
            overlap_bboxes=overlap_bboxes,
            overlap_flag=overlap_flag)
        logits_q = nn.functional.normalize(logits_q, dim=1)

        num_feat_levels = logits_q.shape[0] // (
                batch_size * self.num_pos_per_instance)
        assert (self.num_levels == num_feat_levels)
        logits_q = logits_q.view(batch_size * self.num_pos_per_instance,
                                 num_feat_levels, -1)
        logits_q_patch = torch.cat(logits_q_patch, dim=1)  # batch_size * 4 * feat_dim
        logits_q_patch = nn.functional.normalize(logits_q_patch, dim=-1)
        logits_overlap = nn.functional.normalize(logits_overlap, dim=1)

        img_k = target_data['img']
        img_metas_k = target_data['img_metas']
        gt_bboxes_k = target_data['gt_bboxes']
        gt_labels_k = target_data['gt_labels']

        # compute key features
        with torch.no_grad():  # no gradient to keys
            self._momentum_update_key_encoder()  # update the key encoder

            # shuffle for making use of BN
            shuffle_idx = 'idx' in self.shuffle_data
            shuffle_bbox = 'bbox' in self.shuffle_data
            img_k, shuffle_gt_labels_k, shuffle_gt_bboxes_k, idx_unshuffle = self._batch_shuffle_ddp(
                img_k, gt_labels_k if shuffle_idx else None,
                gt_bboxes_k if shuffle_bbox else None)

            if shuffle_idx:
                gt_labels_k = [each for each in shuffle_gt_labels_k]
            else:
                gt_labels_k = [each for each in gt_labels_k]

            if shuffle_bbox:
                gt_bboxes_k = [each for each in shuffle_gt_bboxes_k]
            else:
                gt_bboxes_k = [each for each in gt_bboxes_k]

            logits_k, _ = self.fwd(
                img=img_k,
                img_metas=img_metas_k,
                gt_labels=gt_labels_k,
                gt_bboxes=gt_bboxes_k,
                backbone=self.backbone_k,
                roi_head=self.roi_head_k,
                neck=self.neck_k,
                pool_with_gt=self.pool_with_gt[1],
                rpn_head=self.rpn_head_k,
                query_encoder=False)
            logits_k = nn.functional.normalize(logits_k, dim=1)
            logits_k = logits_k.view(batch_size, num_feat_levels, -1)
            # logits_k_patch = torch.cat(logits_k_patch, dim=1)  # batch_size * 4 * feat_dim
            # logits_k_patch = logits_k_patch[local_flag]
            # logits_k_patch = nn.functional.normalize(logits_k_patch, dim=-1)

            # undo shuffle
            logits_k = self._batch_unshuffle_ddp(logits_k, idx_unshuffle).view(
                batch_size, num_feat_levels, -1)
            if self.num_pos_per_instance > 1:
                batch_size_k = logits_k.shape[0]
                batch_size_q = logits_q.shape[0]
                assert (batch_size_q //
                        batch_size_k == self.num_pos_per_instance)
                repeated_logits_k = torch.repeat_interleave(
                    logits_k.unsqueeze(1), self.num_pos_per_instance,
                    1).view(-1, num_feat_levels, logits_q.shape[-1])
            else:
                repeated_logits_k = logits_k

        for level_idx in range(num_feat_levels):
            # compute logits
            # Einstein sum is more intuitive
            # positive logits: Nx1
            l_pos = torch.einsum('nc,nc->n', [
                logits_q[:, level_idx, :], repeated_logits_k[:, level_idx, :]
            ]).unsqueeze(-1)
            # negative logits: NxK
            l_neg = torch.einsum('nc,ck->nk', [
                logits_q[:, level_idx, :],
                self.queues[level_idx].clone().detach()
            ])

            # logits: Nx(1+K)
            logits = torch.cat([l_pos, l_neg], dim=1)

            # apply temperature
            logits /= self.T

            # labels: positive key indicators
            labels = torch.zeros(logits.shape[0], dtype=torch.long).cuda()

            # ce loss
            loss_cls = self.level_loss_weights[
                           level_idx] * nn.functional.cross_entropy(logits, labels)
            acc = accuracy(logits, labels)

            losses.update({
                f'loss_cls_{level_idx}': loss_cls,
                f'acc_{level_idx}': acc
            })
            # loss_cls=loss_cls, acc=acc)

        # sim_scores_pred = torch.zeros((batch_size, 4)).cuda()
        logits_q_patch = logits_q_patch[overlap_flag]
        sim_scores = sim_scores[overlap_flag]
        assert logits_q_patch.size(0) == logits_overlap.size(0) == sim_scores.size(0)
        sim_scores_pred_list_q = list()
        num_patch_q = logits_q_patch.shape[1]
        for patch_idx in range(num_patch_q):
            sim_score_pred = torch.einsum('nc,nc->n', [logits_q_patch[:, patch_idx, :],
                                                       logits_overlap])
            sim_scores_pred_list_q.append(sim_score_pred.unsqueeze(1))
        sim_scores_pred = torch.cat(sim_scores_pred_list_q, dim=1)
        # sim_scores_pred = torch.log(sim_scores_pred / sim_scores_pred.sum(dim=1).unsqueeze(1).expand(sim_scores_pred.size()))
        # sim_scores_pred = nn.functional.log_softmax(sim_scores_pred, dim=1)
        # print(sim_scores_pred, sim_scores)
        loss_sim = self.sim_loss_weight * nn.functional.pairwise_distance(sim_scores_pred, sim_scores).mean()
        # loss_sim = - self.sim_loss_weight * torch.mul(sim_scores, sim_scores_pred).sum(dim=1).mean()
        losses.update({
            f'loss_sim': loss_sim
        })

        # dequeue and enqueue
        self._dequeue_and_enqueue(logits_k)

        return losses
