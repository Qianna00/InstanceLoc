#from .bbox_heads import MultiConvFCBBoxInsClsHead
import time

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F

import mmdet
from mmdet.core import bbox2result, bbox2roi, build_assigner, build_sampler
from ..builder import HEADS, build_head, build_roi_extractor
from .base_roi_head import BaseRoIHead
from .test_mixins import BBoxTestMixin, MaskTestMixin
from .moco_standard_roi_head import MomentumRoIPool


@HEADS.register_module()
class MomentumRoIPoolPatch(BaseRoIHead, BBoxTestMixin, MaskTestMixin):

    def __init__(self,
                 bbox_roi_extractor=None,
                 bbox_head=None,
                 with_patch=False,
                 mask_roi_extractor=None,
                 mask_head=None,
                 shared_head=None,
                 train_cfg=None,
                 test_cfg=None
                 ):
        self.with_patch = with_patch
        super(MomentumRoIPoolPatch, self).__init__(bbox_roi_extractor, bbox_head,
                                                   mask_roi_extractor,
                                                   mask_head,
                                                   shared_head,
                                                   train_cfg,
                                                   test_cfg
                                                   )

    def init_bbox_head(self, bbox_roi_extractor, bbox_head):
        num_extractor = bbox_roi_extractor.get('num_extractor', 1)
        ops = nn.ModuleList()
        featmap_strides = bbox_roi_extractor.get('featmap_strides', [-1])

        if num_extractor == 1:
            if 'num_extractor' in bbox_roi_extractor.keys():
                bbox_roi_extractor.pop('num_extractor')
            ops.append(build_roi_extractor(bbox_roi_extractor))
        else:
            bbox_roi_extractor_copy = bbox_roi_extractor.deepcopy()
            assert (num_extractor == len(
                bbox_roi_extractor_copy['featmap_strides']))
            out_size = bbox_roi_extractor_copy['roi_layer'].get('out_size', 7)
            out_channels = bbox_roi_extractor['out_channels']
            for i in range(num_extractor):
                if 'num_extractor' in bbox_roi_extractor_copy.keys():
                    bbox_roi_extractor_copy.pop('num_extractor')
                bbox_roi_extractor_copy['featmap_strides'] = [
                    featmap_strides[i]
                ]
                bbox_roi_extractor_copy[
                    'roi_layer']['out_size'] = out_size if not isinstance(
                        out_size, list) else out_size[i]
                if isinstance(out_channels, list):
                    bbox_roi_extractor_copy['out_channels'] = out_channels[i]
                ops.append(build_roi_extractor(bbox_roi_extractor_copy))
            if self.with_patch:
                if 'num_extractor' in bbox_roi_extractor_copy.keys():
                    bbox_roi_extractor_copy.pop('num_extractor')
                bbox_roi_extractor_copy['featmap_strides'] = [
                    featmap_strides[-1]
                ]
                bbox_roi_extractor_copy[
                    'roi_layer']['out_size'] = out_size * 2 if not isinstance(
                    out_size, list) else out_size[-1] * 2
                if isinstance(out_channels, list):
                    bbox_roi_extractor_copy['out_channels'] = out_channels[-1]
                self.bbox_roi_extractor_patch = build_roi_extractor(bbox_roi_extractor_copy)

        self.bbox_roi_extractor = ops
        self.bbox_head = build_head(bbox_head)

    def init_mask_head(self):
        pass

    def init_assigner_sampler(self):
        pass

    def init_weights(self, pretrained):
        if self.with_shared_head:
            self.shared_head.init_weights(pretrained=pretrained)
        if self.with_bbox:
            for i, bbox_roi_extractor in enumerate(self.bbox_roi_extractor):
                bbox_roi_extractor.init_weights()
            self.bbox_head.init_weights()

    def forward_train(
        self,
        x,
        img_metas,
        proposal_list,
        gt_bboxes,
        gt_labels,
        box_replaced_with_gt=None,
    ):
        if self.with_patch:
            logits, logits_patch = self._bbox_forward_train(
                x,
                proposal_list,
                gt_bboxes,
                gt_labels,
                img_metas,
                box_replaced_with_gt,
            )
            return logits, logits_patch
        else:
            logits = self._bbox_forward_train(
                x,
                proposal_list,
                gt_bboxes,
                gt_labels,
                img_metas,
                box_replaced_with_gt,
            )
            return logits

    def _bbox_forward(self, x, rois, gt_rois=None, box_replaced_with_gt=None):
        if len(self.bbox_roi_extractor) > 1:
            bbox_feats = []
            for i, roi_extractor in enumerate(self.bbox_roi_extractor):
                if box_replaced_with_gt is None:
                    bbox_feat = roi_extractor((x[i], ), rois)
                else:
                    current_rois = gt_rois if box_replaced_with_gt[i] else rois
                    bbox_feat = roi_extractor((x[i], ), current_rois)
                bbox_feats.append(bbox_feat)
            bbox_feats = torch.stack(bbox_feats, 1)
            bs, num_level, dim, shape_w, shape_h = bbox_feats.shape
            bbox_feats = bbox_feats.view(bs * num_level, dim, shape_w, shape_h)
            if self.with_patch:
                if box_replaced_with_gt is None:
                    bbox_feats_patch = self.bbox_roi_extractor_patch((x[-1], ), rois)
                else:
                    current_rois = gt_rois if box_replaced_with_gt[-1] else rois
                    bbox_feats_patch = self.bbox_roi_extractor_patch((x[-1], ), current_rois)
        else:
            bbox_feats = self.bbox_roi_extractor[0](
                x[:self.bbox_roi_extractor[0].num_inputs], rois)

        if self.with_shared_head:
            bbox_feats = self.shared_head(bbox_feats)
            if self.with_patch:
                bbox_feats_patch = self.shared_head(bbox_feats_patch)
        logits, _ = self.bbox_head(bbox_feats)
        if self.with_patch:
            logits_patch = []
            shape_w, shape_h = bbox_feats_patch.shape[-2:]
            shape_w_split = int(shape_w / 2)
            shape_h_split = int(shape_h / 2)
            bbox_feats_patch_list = [bbox_feats_patch[:, :, :shape_w_split, :shape_h_split],
                                     bbox_feats_patch[:, :, shape_w_split:, :shape_h_split],
                                     bbox_feats_patch[:, :, :shape_w_split, shape_h_split:],
                                     bbox_feats_patch[:, :, shape_w_split:, shape_h_split:]]
            for feat in bbox_feats_patch_list:
                logit, _ = self.bbox_head(feat)
                logits_patch.append(logit.unsqueeze(1))
            return logits, logits_patch
        return logits

    def _bbox_forward_train(self, x, sampling_results, gt_bboxes, gt_labels,
                            img_metas, box_replaced_with_gt):
        rois = bbox2roi(sampling_results)
        # GT for other level feats
        if box_replaced_with_gt is not None:
            gt_rois = bbox2roi(gt_bboxes)
        else:
            gt_rois = None

        if self.with_patch:
            logits, logits_patch = self._bbox_forward(x, rois, gt_rois, box_replaced_with_gt)
            return logits, logits_patch
        else:
            logits = self._bbox_forward(x, rois, gt_rois, box_replaced_with_gt)
            return logits


@HEADS.register_module()
class AnotherMomentumRoIPoolPatch(MomentumRoIPool):

    def forward_train(
        self,
        x,
        img_metas,
        proposal_list,
        gt_bboxes,
        gt_labels,
        box_replaced_with_gt=None,
        overlap_bboxes=None,
        overlap_flag=None
    ):
        if overlap_bboxes is not None:
            logits, logits_patch, logits_overlap = self._bbox_forward_train(
                x,
                proposal_list,
                gt_bboxes,
                gt_labels,
                img_metas,
                box_replaced_with_gt,
                overlap_bboxes=overlap_bboxes,
                overlap_flag=overlap_flag)
            return logits, logits_patch, logits_overlap
        else:
            logits, logits_patch = self._bbox_forward_train(
                x,
                proposal_list,
                gt_bboxes,
                gt_labels,
                img_metas,
                box_replaced_with_gt,
                overlap_bboxes=overlap_bboxes)
            return logits, logits_patch

    def _bbox_forward(self, x, rois, gt_rois=None, overlap_rois=None, overlap_flag=None, box_replaced_with_gt=None):
        if len(self.bbox_roi_extractor) > 1:
            bbox_feats = []
            for i, roi_extractor in enumerate(self.bbox_roi_extractor):
                if box_replaced_with_gt is None:
                    bbox_feat = roi_extractor((x[i], ), rois)
                else:
                    current_rois = gt_rois if box_replaced_with_gt[i] else rois
                    bbox_feat = roi_extractor((x[i], ), current_rois)
                bbox_feats.append(bbox_feat)
            bbox_feats = torch.stack(bbox_feats, 1)
            bs, num_level, dim, shape_w, shape_h = bbox_feats.shape
            bbox_feats = bbox_feats.view(bs * num_level, dim, shape_w, shape_h)
        else:
            bbox_feats = self.bbox_roi_extractor[0](
                x[:self.bbox_roi_extractor[0].num_inputs], rois)

        if self.with_shared_head:
            bbox_feats = self.shared_head(bbox_feats)

        num_levels = len(self.bbox_roi_extractor)
        if num_levels > 1:
            bbox_feats_spilt = torch.chunk(bbox_feats, num_levels, dim=0)
            assert len(bbox_feats_spilt) == num_levels
            bbox_feats_extra_branch = bbox_feats_spilt[-1]
        else:
            bbox_feats_extra_branch = bbox_feats

        bbox_feats_patch = nn.functional.interpolate(bbox_feats_extra_branch, scale_factor=2, mode='bilinear')
        logits, _ = self.bbox_head(bbox_feats)

        if overlap_rois is not None:
            bs = overlap_rois.shape[0]
            assert overlap_flag.size(0) == bs
            # logits_overlap = torch.zeros(bs, 128).cuda()
            overlap_feats = self.bbox_roi_extractor[-1]((x[-1], ), overlap_rois)
            overlap_feats = overlap_feats[overlap_flag]
            # overlap_feats with/without shared_head?
            logits_overlap, _ = self.bbox_head(overlap_feats)
            # logits_overlap[overlap_flag] = logits_overlap_

        logits_patch = []
        shape_w, shape_h = bbox_feats_patch.shape[-2:]
        shape_w_split = int(shape_w / 2)
        shape_h_split = int(shape_h / 2)
        bbox_feats_patch_list = [bbox_feats_patch[:, :, :shape_w_split, :shape_h_split],
                                 bbox_feats_patch[:, :, shape_w_split:, :shape_h_split],
                                 bbox_feats_patch[:, :, :shape_w_split, shape_h_split:],
                                 bbox_feats_patch[:, :, shape_w_split:, shape_h_split:]]
        for feat in bbox_feats_patch_list:
            logit, _ = self.bbox_head(feat)
            logits_patch.append(logit.unsqueeze(1))

        if overlap_rois is not None:
            return logits, logits_patch, logits_overlap

        return logits, logits_patch

    def _bbox_forward_train(self, x, sampling_results, gt_bboxes, gt_labels,
                            img_metas, box_replaced_with_gt, overlap_bboxes=None, overlap_flag=None):
        rois = bbox2roi(sampling_results)
        # GT for other level feats
        if box_replaced_with_gt is not None:
            gt_rois = bbox2roi(gt_bboxes)
        else:
            gt_rois = None

        if overlap_bboxes is not None:
            overlap_rois = bbox2roi(overlap_bboxes)
            logits, logits_patch, logits_overlap = self._bbox_forward(x, rois, gt_rois, overlap_rois,
                                                                      overlap_flag, box_replaced_with_gt)
            return logits, logits_patch, logits_overlap

        else:
            logits, logits_patch = self._bbox_forward(x, rois, gt_rois, box_replaced_with_gt)
            return logits, logits_patch
