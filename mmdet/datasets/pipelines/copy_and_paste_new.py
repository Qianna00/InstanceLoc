import base64
import math
import random
import torch

import cv2
import mmcv
import numpy as np

from ..builder import PIPELINES
from .copy_and_paste import CopyAndPaste
from mmdet.core import eval_map, eval_recalls, bbox_overlaps


def softmax(x):
    return np.exp(x) / np.sum(np.exp(x), axis=0)


@PIPELINES.register_module()
class CopyAndPasteNew(CopyAndPaste):

    @staticmethod
    def get_rescale_param(img, ratio, scale=(0.2, 1.0)):
        height = img.shape[0]
        width = img.shape[1]
        area = height * width

        for _ in range(10):
            target_area = random.uniform(*scale) * area
            log_ratio = (math.log(ratio[0]), math.log(ratio[1]))
            aspect_ratio = math.exp(random.uniform(*log_ratio))

            target_width = int(round(math.sqrt(target_area * aspect_ratio)))
            target_height = int(round(math.sqrt(target_area / aspect_ratio)))

            if 0 < target_width <= width and 0 < target_height <= height:
                ymin = random.randint(0, height - target_height)
                xmin = random.randint(0, width - target_width)
                return xmin, ymin, target_height, target_width

        # Fallback to central crop
        in_ratio = float(width) / float(height)
        if in_ratio < min(ratio):
            target_width = width
            target_height = int(round(target_width / min(ratio)))
        elif in_ratio > max(ratio):
            target_height = height
            target_width = int(round(target_height * max(ratio)))
        else:  # whole image
            target_width = width
            target_height = height
        ymin = (height - target_height) // 2
        xmin = (width - target_width) // 2
        return xmin, ymin, target_height, target_width

    @staticmethod
    def get_position_param(crop, size=(256, 256)):
        H, W = size[0], size[1]
        h, w, _ = crop.shape
        sampled_w = int(np.random.randint(0, W - w, 1))
        sampled_h = int(np.random.randint(0, H - h, 1))
        return sampled_h, sampled_w

    def _load_(self, filename):
        img_bytes = self.file_client.get(filename)
        img = mmcv.imfrombytes(img_bytes, flag=self.color_type)
        if self.to_float32:
            img = img.astype(np.float32)
        return img

    def _load_bytes(self, img_str):
        img_bytes = base64.b64decode(img_str)
        img = mmcv.imfrombytes(img_bytes, flag='color', backend='cv2')
        if self.to_float32:
            img = img.astype(np.float32)
        return img

    def load_imgs(self, results):
        if self.feed_bytes:
            self.file_client = None
            base_path = results['img_bytes']
            instance_path_list = results['instance_bytes']
            load_func = self._load_bytes
        else:
            base_path = results['img_info']
            instance_path_list = results['instance_info']
            load_func = self._load_

        if self.file_client is None:
            self.file_client = mmcv.FileClient(**self.file_client_args)

        base_img0 = load_func(base_path[0])
        base_img1 = load_func(base_path[1])
        base_img0 = mmcv.imresize(base_img0, self.base_scale)
        base_img1 = mmcv.imresize(base_img1, self.base_scale)

        foreground_img = load_func(instance_path_list[0])
        bbox = results["coarse_bbox"]
        return base_img0, base_img1, foreground_img, bbox

    def pack(self, q_img, bboxes0, results, background_mask=None):
        # organize results0
        results0 = dict()
        results0['img_prefix'] = ''
        results0['seg_prefix'] = None
        results0['proposal_file'] = None
        results0['bbox_fields'] = []
        results0['mask_fields'] = []
        results0['seg_fields'] = []
        results0['filename'] = results['img_info']
        results0['ins_filename'] = results['instance_info']
        results0['ori_filename'] = results['img_info']
        results0['img'] = q_img
        results0['img_shape'] = q_img.shape
        results0['ori_shape'] = q_img.shape
        # Set initial values for default meta_keys
        results0['pad_shape'] = q_img.shape
        results0['scale_factor'] = 1.0
        num_channels = 1 if len(q_img.shape) < 3 else q_img.shape[2]
        results0['img_norm_cfg'] = dict(
            mean=np.zeros(num_channels, dtype=np.float32),
            std=np.ones(num_channels, dtype=np.float32),
            to_rgb=False)
        results0['img_fields'] = ['img']
        results0['gt_bboxes'] = np.array(bboxes0, dtype=np.float32)
        results0['bbox_fields'].append('gt_bboxes')
        results0['gt_labels'] = results['ins_idx']
        return results0

    def get_crop(self, img, ratio, bbox=None):
        if bbox is not None:
            xmin, ymin, target_height, target_width = bbox
        else:
            xmin, ymin, target_height, target_width = self.get_rescale_param(
                img, ratio)
        crop = mmcv.imcrop(
            img,
            np.array([
                xmin, ymin, xmin + target_width - 1, ymin + target_height - 1
            ]))
        bbox_params = [xmin, ymin, target_height, target_width]
        return crop, bbox_params

    def get_scale(self):
        w = int(np.random.randint(
            self.w_range[0], self.w_range[1],
            1)) if self.w_range[0] < self.w_range[1] else self.w_range[0]
        h = int(np.random.randint(
            self.h_range[0], self.h_range[1],
            1)) if self.h_range[0] < self.h_range[1] else self.h_range[0]
        return (w, h)

    def __call__(self, results):
        # load img from given paths
        base_img0, base_img1, chosen_img, bbox = self.load_imgs(results)

        W, H, C = base_img0.shape
        BASE_SIZE = (W, H)
        q_img = base_img0.copy()
        k_img = base_img1.copy()

        num_imgs = len(chosen_img)

        # Get crop, randomly select a crop as query or key
        r = np.random.uniform()
        if r > 0.5:
            crop1 = self.get_crop(chosen_img, self.ratio, bbox)
            crop2 = self.get_crop(crop1, self.ratio)
        else:
            crop2 = self.get_crop(chosen_img, self.ratio, bbox)
            crop1 = self.get_crop(crop2, self.ratio)
        # crop2 = self.get_crop(chosen_img, self.ratio)

        # Get scale
        sampled_scale1 = self.get_scale()
        sampled_scale2 = self.get_scale()

        # Rescale foreground images
        try:
            crop1 = mmcv.imrescale(crop1, sampled_scale1)
            crop2 = mmcv.imrescale(crop2, sampled_scale2)
        except:
            crop1 = mmcv.imresize(crop1, sampled_scale1)
            crop2 = mmcv.imresize(crop2, sampled_scale2)

        # Sample Location
        sampled_w, sampled_h, _ = crop1.shape
        position_w, position_h = self.get_position_param(crop1, BASE_SIZE)
        bbox0 = [
            position_h, position_w, sampled_h + position_h,
            sampled_w + position_w
        ]
        bboxes0 = [bbox0]
        q_img[position_w:position_w + sampled_w,
              position_h:position_h + sampled_h, :] = crop1

        sampled_w, sampled_h, _ = crop2.shape
        position_w, position_h = self.get_position_param(crop2, BASE_SIZE)
        bbox1 = [
            position_h, position_w, sampled_h + position_h,
            sampled_w + position_w
        ]
        bboxes1 = [bbox1]
        k_img[position_w:position_w + sampled_w,
              position_h:position_h + sampled_h, :] = crop2

        results0 = self.pack(q_img, bboxes0, results)
        results1 = self.pack(k_img, bboxes1, results)
        return [results0, results1]


@PIPELINES.register_module()
class CopyAndPasteNew_(CopyAndPasteNew):

    """def compute_similarity(self, img, xmin, ymin, target_height, target_width):
        base_height = img.shape[0]
        base_width = img.shape[1]
        # area = base_height * base_width
        crop_tl = max((base_width / 2 - xmin) * (ymin + target_height - base_height / 2), 0)
        crop_tr = max((xmin + target_width - base_width / 2) * (ymin + target_height - base_height / 2), 0)
        crop_bl = max((base_width / 2 - xmin) * (base_height / 2 - ymin), 0)
        crop_br = max((xmin + target_width - base_width / 2) * (base_height / 2 - ymin), 0)
        similarity = np.array([crop_tl, crop_tr, crop_bl, crop_br])
        similarity_norm = softmax(similarity)
        return similarity_norm"""

    def __call__(self, results):
        # load img from given paths
        base_img0, base_img1, chosen_img, bbox = self.load_imgs(results)

        W, H, C = base_img0.shape
        BASE_SIZE = (W, H)
        q_img = base_img0.copy()
        k_img = base_img1.copy()

        num_imgs = len(chosen_img)

        # Get crop, randomly select a crop as query or key
        r = np.random.uniform()
        if r > 0.5:
            crop1, _ = self.get_crop(chosen_img, self.ratio, bbox)
            crop2, bbox_params = self.get_crop(crop1, self.ratio)
            global_flag = True
        else:
            crop2, _ = self.get_crop(chosen_img, self.ratio, bbox)
            crop1, bbox_params = self.get_crop(crop2, self.ratio)
            global_flag = False
        # crop2 = self.get_crop(chosen_img, self.ratio)

        # Get scale
        sampled_scale1 = self.get_scale()
        sampled_scale2 = self.get_scale()

        # Rescale foreground images
        try:
            crop1 = mmcv.imrescale(crop1, sampled_scale1)
            crop2 = mmcv.imrescale(crop2, sampled_scale2)
        except:
            crop1 = mmcv.imresize(crop1, sampled_scale1)
            crop2 = mmcv.imresize(crop2, sampled_scale2)

        # Sample Location
        sampled_w, sampled_h, _ = crop1.shape
        position_w, position_h = self.get_position_param(crop1, BASE_SIZE)
        bbox0 = [
            position_h, position_w, sampled_h + position_h,
            sampled_w + position_w
        ]
        bboxes0 = [bbox0]
        q_img[position_w:position_w + sampled_w,
              position_h:position_h + sampled_h, :] = crop1

        sampled_w, sampled_h, _ = crop2.shape
        position_w, position_h = self.get_position_param(crop2, BASE_SIZE)
        bbox1 = [
            position_h, position_w, sampled_h + position_h,
            sampled_w + position_w
        ]
        bboxes1 = [bbox1]
        k_img[position_w:position_w + sampled_w,
              position_h:position_h + sampled_h, :] = crop2

        results0 = self.pack(q_img, bboxes0, results)
        results1 = self.pack(k_img, bboxes1, results)
        return [results0, results1, bbox, bbox_params, global_flag]


@PIPELINES.register_module()
class CopyAndPasteNew2(CopyAndPasteNew):

    """def compute_similarity(self, img, xmin, ymin, target_height, target_width):
        base_height = img.shape[0]
        base_width = img.shape[1]
        # area = base_height * base_width
        crop_tl = max((base_width / 2 - xmin) * (ymin + target_height - base_height / 2), 0)
        crop_tr = max((xmin + target_width - base_width / 2) * (ymin + target_height - base_height / 2), 0)
        crop_bl = max((base_width / 2 - xmin) * (base_height / 2 - ymin), 0)
        crop_br = max((xmin + target_width - base_width / 2) * (base_height / 2 - ymin), 0)
        similarity = np.array([crop_tl, crop_tr, crop_bl, crop_br])
        similarity_norm = softmax(similarity)
        return similarity_norm"""

    def load_imgs(self, results):
        if self.feed_bytes:
            self.file_client = None
            base_path = results['img_bytes']
            instance_path_list = results['instance_bytes']
            load_func = self._load_bytes
        else:
            base_path = results['img_info']
            instance_path_list = results['instance_info']
            load_func = self._load_

        if self.file_client is None:
            self.file_client = mmcv.FileClient(**self.file_client_args)

        # load imgs H, W, C

        base_img0 = load_func(base_path[0])
        base_img1 = load_func(base_path[1])
        base_img0 = mmcv.imresize(base_img0, self.base_scale)
        base_img1 = mmcv.imresize(base_img1, self.base_scale)

        foreground_img = load_func(instance_path_list[0])
        return base_img0, base_img1, foreground_img

    def __call__(self, results):
        # load img from given paths
        base_img0, base_img1, chosen_img = self.load_imgs(results)

        H, W, C = base_img0.shape
        BASE_SIZE = (H, W)
        q_img = base_img0.copy()
        k_img = base_img1.copy()

        num_imgs = len(chosen_img)

        # Get crop, randomly select a crop as query or key
        crop1, bbox_params1 = self.get_crop(chosen_img, self.ratio)
        crop2, bbox_params2 = self.get_crop(chosen_img, self.ratio)

        # bbox_overlap(bbox_params1, bbox_params2)

        # get overlap params
        overlap_params1, overlap_params2, flag = self.get_overlap(bbox_params1, bbox_params2)

        # Get scale
        sampled_scale1 = self.get_scale()
        sampled_scale2 = self.get_scale()

        # Rescale foreground images
        try:
            crop1 = mmcv.imrescale(crop1, sampled_scale1)
            crop2 = mmcv.imrescale(crop2, sampled_scale2)
        except:
            crop1 = mmcv.imresize(crop1, sampled_scale1)
            crop2 = mmcv.imresize(crop2, sampled_scale2)

        # Sample Location
        sampled_h, sampled_w, _ = crop1.shape
        position_h, position_w = self.get_position_param(crop1, BASE_SIZE)
        bbox0 = [
            position_w, position_h,
            sampled_w + position_w,
            sampled_h + position_h
        ]
        bboxes0 = [bbox0]
        q_img[position_h:position_h + sampled_h,
              position_w:position_w + sampled_w, :] = crop1

        # get overlap params in q_img
        if flag:
            x1_ = int(round(overlap_params1[0] * sampled_w)) + position_w
            y1_ = int(round(overlap_params1[1] * sampled_h)) + position_h
            w1_ = int(round(overlap_params1[2] * sampled_w))
            h1_ = int(round(overlap_params1[3] * sampled_h))
            overlap_bboxes0 = [x1_, y1_, x1_ + w1_, y1_ + h1_]
        else:
            overlap_bboxes0 = [0., 0., 0., 0.]
        overlap_bboxes0 = [overlap_bboxes0]
        # flag = [flag]

        sampled_h, sampled_w, _ = crop2.shape
        position_h, position_w = self.get_position_param(crop2, BASE_SIZE)
        bbox1 = [
            position_w, position_h,
            sampled_w + position_w,
            sampled_h + position_h
        ]
        bboxes1 = [bbox1]
        k_img[position_h:position_h + sampled_h,
              position_w:position_w + sampled_w, :] = crop2

        # get overlap params in k_img
        overlap_bboxes1 = [0., 0., 0., 0.]
        # x2_ = overlap_params2[0] * sampled_w + position_w
        # y2_ = overlap_params2[1] * sampled_h + position_h
        # w2_ = overlap_params2[2] * sampled_w
        # h2_ = overlap_params2[3] * sampled_h

        # overlap_bboxes1 = [x2_, y2_, x2_ + w2_, y2_ + h2_]
        overlap_bboxes1 = [overlap_bboxes1]

        results0 = self.pack_(q_img, bboxes0, overlap_bboxes0, flag, results)
        results1 = self.pack_(k_img, bboxes1, overlap_bboxes1, flag, results)
        return [results0, results1]

    def get_overlap(self, bbox_params1, bbox_params2):
        x1, y1, h1, w1 = bbox_params1
        x2, y2, h2, w2 = bbox_params2
        x_ = max(x1, x2)
        y_ = max(y1, y2)
        w_ = abs(min(x1 + w1, x2 + w2) - x_)
        h_ = abs(min(y1 + h1, y2 + h2) - y_)

        if x1 + w1 <= x2 or x2 + w2 <= x1 or y1 + h1 <= y2 or y2 + h2 <= y1:
            return [0., 0., 0., 0.], [0., 0., 0., 0.], False

        x1_ = (x_ - x1) / w1
        y1_ = (y_ - y1) / h1
        w1_ = w_ / w1
        h1_ = h_ / h1

        x2_ = (x_ - x2) / w2
        y2_ = (y_ - y2) / h2
        w2_ = w_ / w2
        h2_ = h_ / h2

        overlap_params1 = [x1_, y1_, w1_, h1_]
        overlap_params2 = [x2_, y2_, w2_, h2_]
        # overlap_params = x_, y_, w_, h_

        return overlap_params1, overlap_params2, True

    def pack_(self, q_img, bboxes0, overlap_bboxes0, flag, results, background_mask=None):
        # organize results0
        results0 = dict()
        results0['img_prefix'] = ''
        results0['seg_prefix'] = None
        results0['proposal_file'] = None
        results0['bbox_fields'] = []
        results0['mask_fields'] = []
        results0['seg_fields'] = []
        results0['filename'] = results['img_info']
        results0['ins_filename'] = results['instance_info']
        results0['ori_filename'] = results['img_info']
        results0['img'] = q_img
        results0['img_shape'] = q_img.shape
        results0['ori_shape'] = q_img.shape
        # Set initial values for default meta_keys
        results0['pad_shape'] = q_img.shape
        results0['scale_factor'] = 1.0
        num_channels = 1 if len(q_img.shape) < 3 else q_img.shape[2]
        results0['img_norm_cfg'] = dict(
            mean=np.zeros(num_channels, dtype=np.float32),
            std=np.ones(num_channels, dtype=np.float32),
            to_rgb=False)
        results0['img_fields'] = ['img']
        results0['gt_bboxes'] = np.array(bboxes0, dtype=np.float32)
        results0['overlap_bboxes'] = np.array(overlap_bboxes0, dtype=np.float32)
        results0['bbox_fields'].append('gt_bboxes')
        results0['gt_labels'] = results['ins_idx']
        results0['overlap_flag'] = flag
        return results0
