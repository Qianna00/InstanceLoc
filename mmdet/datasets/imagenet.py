import csv
import io
import math
import os.path as op
import random

import cv2
import mmcv
import torch
import numpy as np
from mmcv.utils import build_from_cfg
from torch.utils.data import Dataset

from mmdet.core import eval_map, eval_recalls, bbox_overlaps
from .builder import DATASETS, PIPELINES
from .pipelines import Compose
from mmcv.parallel import DataContainer as DC
from .pipelines.formating import to_tensor


@DATASETS.register_module()
class ImageNetDataset(Dataset):
    CLASSES = None

    def __init__(
        self,
        ann_file,
        pipeline,
        preprocess,
        data_root=None,
        img_prefix='',
        seg_prefix=None,
    ):

        self.ann_file = ann_file
        self.data_root = data_root
        self.img_prefix = img_prefix

        self.data_infos = self.load_annotations(ann_file)
        self.flag = np.ones(len(self), dtype=np.uint8)

        self.preprocess_pipeline = build_from_cfg(preprocess, PIPELINES)
        self.pipeline = Compose(pipeline)

    def __len__(self):
        return len(self.data_infos)

    def load_annotations(self, ann_file):
        txt_ptr = open(ann_file)
        txt = txt_ptr.readlines()
        all_imgs = [
            # op.join(self.img_prefix,
                    each.strip().split()[0] for each in txt
        ]
        txt_ptr.close()
        return all_imgs

    def pre_pipeline(self, results):
        results['img_prefix'] = self.img_prefix
        results['seg_prefix'] = self.seg_prefix
        results['proposal_file'] = None
        results['bbox_fields'] = []
        results['mask_fields'] = []
        results['seg_fields'] = []

    def __getitem__(self, idx):
        return self.prepare_train_img(idx)

    def prepare_train_img(self, idx):
        # Randomly choose a background image from the whole dataset
        base_idx = int(np.random.randint(0, len(self.data_infos), 1))
        base_info = self.data_infos[base_idx]

        base_idx2 = int(np.random.randint(0, len(self.data_infos), 1))
        base_info2 = self.data_infos[base_idx2]
        base_info = [base_info, base_info2]

        # Load the current image as the foreground image
        current_info = self.data_infos[idx]
        instance_info = [current_info]
        instance_idx = [idx]

        # Compose data
        results = dict(img_info=base_info, instance_info=instance_info)
        results['ins_idx'] = instance_idx

        # Copy and paste foreground image onto two background images
        results0, results1 = self.preprocess_pipeline(results)

        # Augment two synthetic images
        results = self.pipeline(results0)
        results1 = self.pipeline(results1)
        results['target_data'] = results1
        return results


@DATASETS.register_module()
class MarvelDataset(Dataset):
    CLASSES = None

    def __init__(
            self,
            background_data,
            ann_file,
            pipeline,
            preprocess,
            data_root=None,
            img_prefix='',
            seg_prefix=None,
    ):
        self.ann_file = ann_file
        self.data_root = data_root
        self.img_prefix = img_prefix

        self.data_infos = self.load_annotations(ann_file)
        self.flag = np.ones(len(self), dtype=np.uint8)

        self.preprocess_pipeline = build_from_cfg(preprocess, PIPELINES)
        self.pipeline = Compose(pipeline)
        self.background_dataset = build_from_cfg(background_data, DATASETS)

    def __len__(self):
        return len(self.data_infos)

    def load_annotations(self, ann_file):
        txt_ptr = open(ann_file)
        txt = txt_ptr.readlines()
        all_imgs = [each.strip().split(',')[-1] for each in txt]
        txt_ptr.close()
        return all_imgs

    def pre_pipeline(self, results):
        results['img_prefix'] = self.img_prefix
        results['seg_prefix'] = self.seg_prefix
        results['proposal_file'] = None
        results['bbox_fields'] = []
        results['mask_fields'] = []
        results['seg_fields'] = []

    def __getitem__(self, idx):
        return self.prepare_train_img(idx)

    def prepare_train_img(self, idx):
        # Randomly choose a background image from the whole dataset
        base_idx = int(np.random.randint(0, len(self.background_dataset.data_infos), 1))
        # base_idx = int(np.random.randint(0, len(self.data_infos), 1))
        base_info = self.background_dataset.data_infos[base_idx]
        # base_info = self.data_infos[base_idx]

        base_idx2 = int(np.random.randint(0, len(self.background_dataset.data_infos), 1))
        # base_idx2 = int(np.random.randint(0, len(self.data_infos), 1))
        base_info2 = self.background_dataset.data_infos[base_idx2]
        # base_info2 = self.data_infos[base_idx2]
        base_info = [base_info, base_info2]

        # Load the current image as the foreground image
        current_info = self.data_infos[idx]
        instance_info = [current_info]
        instance_idx = [idx]

        # Compose data
        results = dict(img_info=base_info, instance_info=instance_info)
        results['ins_idx'] = instance_idx

        # Copy and paste foreground image onto two background images
        results0, results1 = self.preprocess_pipeline(results)

        # Augment two synthetic images
        results = self.pipeline(results0)
        results1 = self.pipeline(results1)
        results['target_data'] = results1
        return results


@DATASETS.register_module()
class MarvelDatasetBBox(Dataset):
    CLASSES = None

    def __init__(
            self,
            background_data,
            ann_file,
            pipeline,
            preprocess,
            data_root=None,
            img_prefix='',
            seg_prefix=None,
    ):
        self.ann_file = ann_file
        self.data_root = data_root
        self.img_prefix = img_prefix

        self.data_infos, self.bboxes = self.load_annotations(ann_file)
        self.flag = np.ones(len(self), dtype=np.uint8)

        self.preprocess_pipeline = build_from_cfg(preprocess, PIPELINES)
        self.pipeline = Compose(pipeline)
        self.background_dataset = build_from_cfg(background_data, DATASETS)

    def __len__(self):
        return len(self.data_infos)

    def load_annotations(self, ann_file):
        txt_ptr = open(ann_file)
        txt = txt_ptr.readlines()
        all_imgs = [each.split(',')[5] for each in txt]
        bboxes = [[int(each.split(',')[-4]), int(each.split(',')[-3]),
                   int(each.split(',')[-1]), int(each.split(',')[-2])]
                  for each in txt]
        txt_ptr.close()
        return all_imgs, bboxes

    def pre_pipeline(self, results):
        results['img_prefix'] = self.img_prefix
        results['seg_prefix'] = self.seg_prefix
        results['proposal_file'] = None
        results['bbox_fields'] = []
        results['mask_fields'] = []
        results['seg_fields'] = []

    def __getitem__(self, idx):
        return self.prepare_train_img(idx)

    def prepare_train_img(self, idx):
        # Randomly choose a background image from the whole dataset
        base_idx = int(np.random.randint(0, len(self.background_dataset.data_infos), 1))
        # base_idx = int(np.random.randint(0, len(self.data_infos), 1))
        base_info = self.background_dataset.data_infos[base_idx]
        # base_info = self.data_infos[base_idx]

        base_idx2 = int(np.random.randint(0, len(self.background_dataset.data_infos), 1))
        # base_idx2 = int(np.random.randint(0, len(self.data_infos), 1))
        base_info2 = self.background_dataset.data_infos[base_idx2]
        # base_info2 = self.data_infos[base_idx2]
        base_info = [base_info, base_info2]

        # Load the current image as the foreground image
        current_info = self.data_infos[idx]
        instance_info = [current_info]
        instance_idx = [idx]

        # Compose data
        results = dict(img_info=base_info, instance_info=instance_info)
        results['ins_idx'] = instance_idx
        results['coarse_bbox'] = self.bboxes[idx]

        # Copy and paste foreground image onto two background images
        results0, results1, bbox_global, bbox_local, global_flag = self.preprocess_pipeline(results)

        sim_scores = self.compute_sim_scores(bbox_global, bbox_local)

        # Augment two synthetic images
        results = self.pipeline(results0)
        results1 = self.pipeline(results1)
        results['target_data'] = results1
        results['sim_scores'] = DC(sim_scores, stack=True, pad_dims=None)
        results['global_flag'] = global_flag

        return results

    def compute_sim_scores(self, bbox_global, bbox_local):
        height, width = bbox_global[2:]
        bbox_tl = [0, height / 2, width / 2, height]
        bbox_tr = [width / 2, height / 2, width, height]
        bbox_bl = [0, 0, width / 2, height / 2]
        bbox_br = [width / 2, 0, width, height / 2]
        bboxes1 = torch.tensor([bbox_tl, bbox_tr, bbox_bl, bbox_br])
        xmin, ymin, h, w = bbox_local
        bboxes2 = torch.tensor([xmin, ymin, xmin + w, ymin + h]).unsqueeze(0)
        ious = bbox_overlaps(bboxes1.float(), bboxes2.float()).squeeze()
        assert ious.sum() != 0
        similarities = torch.softmax(ious, dim=0)

        return similarities


@DATASETS.register_module()
class MarvelDatasetSim(Dataset):
    CLASSES = None

    def __init__(
            self,
            background_data,
            ann_file,
            pipeline,
            preprocess,
            data_root=None,
            img_prefix='',
            seg_prefix=None,
    ):
        self.ann_file = ann_file
        self.data_root = data_root
        self.img_prefix = img_prefix

        self.data_infos = self.load_annotations(ann_file)
        self.flag = np.ones(len(self), dtype=np.uint8)

        self.preprocess_pipeline = build_from_cfg(preprocess, PIPELINES)
        self.pipeline = Compose(pipeline)
        self.background_dataset = build_from_cfg(background_data, DATASETS)

    def __len__(self):
        return len(self.data_infos)

    def load_annotations(self, ann_file):
        txt_ptr = open(ann_file)
        txt = txt_ptr.readlines()
        all_imgs = [each.split(',')[5] for each in txt]
        txt_ptr.close()
        return all_imgs

    def pre_pipeline(self, results):
        results['img_prefix'] = self.img_prefix
        results['seg_prefix'] = self.seg_prefix
        results['proposal_file'] = None
        results['bbox_fields'] = []
        results['mask_fields'] = []
        results['seg_fields'] = []

    def __getitem__(self, idx):
        return self.prepare_train_img(idx)

    def prepare_train_img(self, idx):
        # Randomly choose a background image from the whole dataset
        base_idx = int(np.random.randint(0, len(self.background_dataset.data_infos), 1))
        # base_idx = int(np.random.randint(0, len(self.data_infos), 1))
        base_info = self.background_dataset.data_infos[base_idx]
        # base_info = self.data_infos[base_idx]

        base_idx2 = int(np.random.randint(0, len(self.background_dataset.data_infos), 1))
        # base_idx2 = int(np.random.randint(0, len(self.data_infos), 1))
        base_info2 = self.background_dataset.data_infos[base_idx2]
        # base_info2 = self.data_infos[base_idx2]
        base_info = [base_info, base_info2]

        # Load the current image as the foreground image
        current_info = self.data_infos[idx]
        instance_info = [current_info]
        instance_idx = [idx]

        # Compose data
        results = dict(img_info=base_info, instance_info=instance_info)
        results['ins_idx'] = instance_idx

        # Copy and paste foreground image onto two background images
        results0, results1 = self.preprocess_pipeline(results)

        bboxes0 = results0['gt_bboxes']
        # bboxes1 = results1['gt_bboxes']
        overlap_bboxes0 = results0['overlap_bboxes']
        # overlap_bboxes1 = results1['overlap_bboxes']

        sim_scores = self.compute_sim_scores(bboxes0, overlap_bboxes0)
        # sim_scores1 = self.compute_sim_scores(bboxes1, overlap_bboxes1)

        # Augment two synthetic images
        results = self.pipeline(results0)
        results1 = self.pipeline(results1)
        results['target_data'] = results1
        results['sim_scores'] = DC(sim_scores, stack=True, pad_dims=None)

        return results

    def compute_sim_scores(self, bbox_global, bbox_local):
        # height, width = bbox_global[2:]
        x1 = bbox_global[:, 0]
        y1 = bbox_global[:, 1]
        x2 = bbox_global[:, 2]
        y2 = bbox_global[:, 3]
        height = y2 - y1
        width = x2 - x1
        # area = torch.tensor(np.multiply(height, width)).cuda()
        bbox_tl = [x1, y1 + int(np.round(height / 2)), x1 + int(np.round(width / 2)), y2]
        bbox_tr = [x1 + int(np.round(width / 2)), y1 + int(np.round(height / 2)), x2, y2]
        bbox_bl = [x1, y1, x1 + int(np.round(width / 2)), y1 + int(np.round(height / 2))]
        bbox_br = [x1 + int(np.round(width / 2)), y1, x2, y1 + int(np.round(height / 2))]
        bboxes1 = torch.tensor([bbox_tl, bbox_tr, bbox_bl, bbox_br]).squeeze()
        bboxes2 = torch.tensor(bbox_local)
        # print(bbox_global, bboxes1, bboxes2)
        ious = self.overlap(bboxes1.float(), bboxes2.float()).squeeze()
        similarities = ious
        # assert ious.sum() != 0
        # similarities = torch.softmax(ious, dim=0)
        # similarities = ious / ious.sum()

        return similarities

    def overlap(self, bboxes1, bboxes2, is_aligned=False, eps=1e-6):
        assert (bboxes1.size(-1) == 4 or bboxes1.size(0) == 0)
        assert (bboxes2.size(-1) == 4 or bboxes2.size(0) == 0)

        rows = bboxes1.size(0)
        cols = bboxes2.size(0)
        area0 = (bboxes1[0, 2] - bboxes1[0, 0]) * (bboxes1[0, 3] - bboxes1[0, 1])
        area = (bboxes2[:, 2] - bboxes2[:, 0]) * (bboxes2[:, 3] - bboxes2[:, 1])
        if is_aligned:
            assert rows == cols

        if rows * cols == 0:
            return bboxes1.new(rows, 1) if is_aligned else bboxes1.new(rows, cols)

        if is_aligned:
            lt = torch.max(bboxes1[:, :2], bboxes2[:, :2])  # [rows, 2]
            rb = torch.min(bboxes1[:, 2:], bboxes2[:, 2:])  # [rows, 2]

            wh = (rb - lt).clamp(min=0)  # [rows, 2]
            overlap = wh[:, 0] * wh[:, 1]

        else:
            lt = torch.max(bboxes1[:, None, :2], bboxes2[:, :2])  # [rows, cols, 2]
            rb = torch.min(bboxes1[:, None, 2:], bboxes2[:, 2:])  # [rows, cols, 2]

            wh = (rb - lt).clamp(min=0)  # [rows, cols, 2]
            overlap = wh[:, :, 0] * wh[:, :, 1]

        area = torch.max(area, area0)
        eps = area.new_tensor([eps])
        union = torch.max(area, eps)
        ious = overlap / union
        return ious
